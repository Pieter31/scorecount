import React, { Component } from 'react';
import {ActivityIndicator,View} from 'react-native';
import {firebase} from '@react-native-firebase/auth';
import {createStackNavigator} from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {GoogleSignin} from 'react-native-google-signin';

import {Card,CardSection} from './components';

import Login from './screens/Login';
import Register from './screens/Register';
import Main from './screens/Main';
import Shows from './screens/Shows';
import Accounts from './screens/Account';
import addShows from './screens/addShows';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

class App extends Component{
    state = {loading: true, loggedIn: null}

    componentDidMount(){
        firebase.auth().onAuthStateChanged((user)=>{
          if (user) {
            this.setState({loggedIn: true, loading: false});
          }else{
            this.setState({loggedIn:false, loading: false});
          }
        });
    
        GoogleSignin.configure({
          scopes: [],
          webClientId: '834202577589-hlb8ik1jb20gb5euhkka2nar4729t5sf.apps.googleusercontent.com',
          offlineAccess: true,
          hostedDomain: '',
          forceConsentPrompt: true,
        });
    }

    render (){

        if (this.state.loading){ //loading Splash

            return(
                <View style={styles.spinnerStyle} >
                    <ActivityIndicator size="large" />
                </View>
            )

        } else { //Navigation Stack

            return (

                <NavigationContainer>

                    {this.state.loggedIn ==false ? (
                        //Not loggedIn stack
                        <Stack.Navigator>
                            <Stack.Screen name ='Login' component={Login} />
                            <Stack.Screen name = 'Register' component = {Register} />
                        </Stack.Navigator> 
                    ):( 
                        // loggedIn stack
                        <Drawer.Navigator>
                            <Drawer.Screen name ='Home' component={Main}/>
                            <Drawer.Screen name ='Shows' component={Shows}/>
                            <Drawer.Screen name ='Add/Remove Shows' component={addShows}/>
                            <Drawer.Screen name ='Accounts' component={Accounts}/>
                        </Drawer.Navigator>
                    )}

                </NavigationContainer>

            );

        }
    }
}

const styles ={
    spinnerStyle: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export default App;