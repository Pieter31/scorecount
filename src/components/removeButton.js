import React from 'react';
import {StyleSheet, TouchableOpacity,Text,View} from 'react-native';

const RemoveButton=({onPress})=> {

    return(
        <TouchableOpacity onPress = {(onPress)} style={styles.Button}>
            <Text style={styles.textStyle}>-</Text>
        </TouchableOpacity>
    )

}

export {RemoveButton};

const styles = StyleSheet.create({
    Button:{
        alignItems: 'center',
        backgroundColor: '#f00707',
        borderColor: '#000000',
        borderRadius: 100,
        height: 45,
        width: 45,
        borderWidth: 1,
        marginLeft: 5,
        marginRight: 5
    },

    textStyle:{
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        fontSize: 25,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10 
    }
})