import React,{Component} from 'react';
import {Button, ActivityIndicator, Text, Image} from 'react-native';
import {Card,CardSection} from '../components'
import {GoogleSignin} from 'react-native-google-signin';
import {firebase} from '@react-native-firebase/auth';
// import FastImage from 'react-native-fast-image';

class Accounts extends Component{
    state ={errorMessage: null, loading: false, currentUser:false, GoogleUser:false}

    componentDidMount() {
        const {currentUser} =firebase.auth();
        this.setState({currentUser});

        const {GoogleUser} = GoogleSignin.isSignedIn();
        this.setState({GoogleUser});
    }

    onLogout = () => {

        firebase
          .auth()
          .signOut()
          .then(this.setState({loading:true}))
          .catch(error => this.setState({ errorMessage: error.message,loading:false}))
        ;

        if (this.state.GoogleUser == true){
            GoogleSignin.revokeAccess();
            GoogleSignin.signOut();
        }
    };

    onDelete = () => {
        const {currentUser} = this.state
        const ref = firebase.database().ref('Users/')

        ref.once('value').then((snapshot) =>{
            snapshot.forEach(item =>{

                console.log(item.val().email)

                if(currentUser.email==item.val().email) {
                    console.log('match')
                    this.dbDelete(item.key)
                }

            })
        })

        firebase
            .auth()
            .currentUser
            .delete()
            .then(this.setState({loading:true}))
            .catch(error => this.setState({ errorMessage: error.message,loading:false}))

    }

    dbDelete(key) {
        firebase.database().ref('Users').child(''+key).remove()
        console.log(key)
    }

    getUserData = () => {
        console.log(this.state.currentUser)
    }

    renderDelete() {

        if (this.state.loading) {

            return(
                <CardSection>
                    <ActivityIndicator size = "small" />
                </CardSection>
            )
        } else {

            return(
                <CardSection>
                    <Button
                        title = "Delete Account"
                        onPress={this.onDelete.bind(this)}
                    />
                </CardSection>
            )
        }
    }

    renderButton() {

        if (this.state.loading) {
            return (
                <CardSection>
                    <ActivityIndicator size = "small" />
                </CardSection>
            )
        } else {
            return (
                <CardSection>
                    <Button 
                        title="Sign Out"
                        onPress={this.onLogout.bind(this)}
                    />
                </CardSection>
            )
        }
    }

    render() {
        const{currentUser} = this.state

        return(

            <Card>

                {this.state.errorMessage &&
                    <CardSection>
                        <Text style={{ color: 'red' }}>
                            {this.state.errorMessage}
                        </Text>
                    </CardSection>
                }

                <CardSection>
                    <Button
                        title="get User Data"
                        onPress={this.getUserData}
                    />
                </CardSection>

                {this.renderButton()}

                {this.renderDelete()}

            </Card>

        )

    }

}

export default Accounts;