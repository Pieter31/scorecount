import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Button, ActivityIndicator } from 'react-native';
import {CardSection,Card} from '../components'
import {firebase} from '@react-native-firebase/auth';
import {GoogleSignin,GoogleSigninButton} from 'react-native-google-signin';
import database from '@react-native-firebase/database';




class Login extends Component{
  state = {match: false, email: '', password: '', errorMessage: null, loading: false,credentails: null}

  onGoogle = () => {

  GoogleSignin.signIn()
    .then((data)=>{

      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);

      return (
        firebase
          .auth()
          .signInWithCredential(credential)
          .then(
            this.testUser
          )
      )

    })
    .catch((error) => {
      const { code, message } = error;
    });

  }

  onLogin = () => {
    const { email, password } = this.state;
    
    if (email == ''||password=='') {

      this.setState({errorMessage: "Email or Password is blank"})

    }
    else {
      
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(this.setState({loading: true}))
        .catch(
          error => this.setState({ errorMessage: error.message,loading: false})
        )

    }

  }

  testUser = () =>{
    const {currentUser} = firebase.auth();
    var match = false;

    const ref = firebase.database().ref('Users/')
    console.log(ref)

    ref.once('value').then((snapshot)=>{
      snapshot.forEach(item =>{
        const temp =item.val();
        console.log(temp)

        if (temp.email == currentUser.email){
          match=true
        }

      })
      if (match==false) {
        console.log(match)
        this.addData(currentUser.email, currentUser.displayName)
      }
    })
    
  }

  addData(email, displayName) {

    firebase.database().ref('Users/').push({
      email,
      displayName
    }).then ((data)=>{
      console.log('data',data)
    }).catch ((error)=>{
      console.log('error',error)
    })

  }

  renderButton() {

    if (this.state.loading) {
        return (

            <CardSection>
                <ActivityIndicator size = "small" />
            </CardSection>

        )
    }
    else{
      return(

        <CardSection>
          <Button 
           title="Login" 
            onPress={this.onLogin} 
          />
        </CardSection>

      );
    }
  };

  render(){

    return (
        <Card>

        <CardSection>

            {this.state.errorMessage &&
                <CardSection>
                    <Text style={{ color: 'red' }}>
                        {this.state.errorMessage}
                    </Text>
                </CardSection>
            }

            <CardSection>
                <TextInput
                    style={styles.textInput}
                    autoCapitalize="none"
                    placeholder="Email"
                    onChangeText={email => this.setState({ email })}
                    value={this.state.email}
                />

                <TextInput
                    secureTextEntry
                    style={styles.textInput}
                    autoCapitalize="none"
                    placeholder="Password"
                    onChangeText={password => this.setState({ password })}
                    value={this.state.password}
                />
            </CardSection>
      
            {this.renderButton()}

            <CardSection>
                <Button
                    title="Don't have an account? Sign Up"
                    onPress={() => this.props.navigation.navigate('Register')}
                />
            </CardSection>

        </CardSection>

        <CardSection>

            <CardSection>
                <Text>Or Sign In with Google</Text>
            </CardSection>

            <CardSection>
                <GoogleSigninButton
                style={{ width: 192, height: 48 }}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={this.onGoogle}
                />
            </CardSection>

        </CardSection>

      </Card>
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})

export default Login;