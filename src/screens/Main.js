import React, {Component} from 'react';
import {View, Text,FlatList, StyleSheet} from 'react-native';
import {firebase} from '@react-native-firebase/app';

const DATA = [
    {
        showID: '001',
        showTitle: 'The Bachelor',
        user1: 'Admin',
        user2: 'Test',
        user1Score: '1',
        user2Score: '2'
    },
    {
        showID: '002',
        showTitle: 'Survivor COVID-19',
        user1: 'Test',
        user2: 'Admin',
        user1Score: '2',
        user2Score: '4' 
    },
    {
        showID: '003',
        showTitle: 'Shark Tank',
        user1: 'Admin',
        user2: 'Test',
        user1Score: '0',
        user2Score: '1'
    },
    // {
    //     showID: '004',
    //     showTitle: 'Fokken iets',
    //     user1: 'Admin',
    //     user2: 'Test',
    //     user1Score: '10',
    //     user2Score: '10'
    // },
    // {

    //     showID: '005',
    //     showTitle: 'Something',
    //     user1: 'Admin',
    //     user2: 'Ass',
    //     user1Score: 6969,
    //     user2Score: 420420
    // },
    {

        showID: '006',
        showTitle: 'test',
        user1: 'test',
        user2: 'Admin',
        user1Score:'10',
        user2Score:'11'
    }
]

class Main extends Component{
    state = {currentUser: null}

    componentDidMount() {
        const {currentUser} = firebase.auth()
        this.setState({currentUser})
    }

    render() {
        const { currentUser} = this.state
        return(
            
               <View style={styles.viewContainer}>

                    <FlatList
                        data={DATA}
                        renderItem={({item})=>
                            <View style={styles.itemContainer}>
                                <View style={styles.titleContainer}>
                                    <Text>{item.showTitle}</Text>
                                </View>
                                <View style={styles.scoreContainer}>
                                    <Text>{item.user1}</Text>
                                    <Text>{item.user1Score}</Text>
                                </View>
                                <View style={styles.scoreContainer}>
                                    <Text>{item.user2}</Text>
                                    <Text>{item.user2Score}</Text>
                                </View>
                            </View>
                        }
                        keyExtractor={item => item.showID}
                    />
                
                </View> 
            
        )

    }
}

export default Main;

const styles = StyleSheet.create ({
    viewContainer:{
        flex: 1,
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 5
    },
    itemContainer:{
        flex:2,
        flexDirection:'row',
        alignSelf:'stretch',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,
        padding: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    scoreContainer:{
        flex: 1,
        flexDirection: 'column',
        alignSelf:'flex-end',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,
        padding: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    titleContainer:{
        flex:2,
        alignSelf:'flex-start'
    }
})