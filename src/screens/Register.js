import React, { Component } from 'react'
import { StyleSheet, Text, TextInput,Button, ActivityIndicator } from 'react-native'
import {Card,CardSection} from '../components';
import {firebase} from '@react-native-firebase/auth';

class Register extends Component{
  state = { email: '', password: '', displayName: '', errorMessage: null,loading: false }

  onRegister = () => {
    const { email, password,displayName } = this.state

    if (email==''||password=='') {
          
      this.setState({errorMessage: "Email or Password is blank"})

    } else {
      
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(
          (userCredentials)=>{
            if(userCredentials.user){
              userCredentials.user.updateProfile({
                displayName: displayName
              }).then((s)=> {
                this.setState({loading:true}),
                this.addData(email,displayName)
              })
            }
        })
        .catch(error => this.setState({ errorMessage: error.message,loading: false}))
      ;

    }

  }

  addData(email,displayName) {
    firebase.database().ref('Users/').push({
      email,
      displayName
    }).then ((data)=>{
          console.log('data',data)
        }).catch ((error)=>{
          console.log('error',error)
        })
  }

  renderButton(){

    if (this.state.loading) {

        return (

            <CardSection>
                <ActivityIndicator size = "small" />
            </CardSection>
    
          )
          
    } else {

      return(
          
        <CardSection>
          <Button
            title="Register"
            onPress={this.onRegister}
          />
        </CardSection>

      );
          
    }
  }

  render(){

    return(

      <Card>
        <CardSection>

        {this.state.errorMessage &&
          <CardSection>
            <Text style={{ color: 'red' }}>
              {this.state.errorMessage}
            </Text>
          </CardSection>
        }

        <CardSection>
          <TextInput
            placeholder="Name"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={displayName => this.setState({ displayName })}
            value={this.state.displayName}
          />

          <TextInput
            placeholder="Email"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />

          <TextInput
            secureTextEntry
            placeholder="Password"
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
        </CardSection>

        {this.renderButton()}

        <CardSection>
          <Button
            title="Already have an account? Login"
            onPress={() => this.props.navigation.navigate('Login')}
          />
        </CardSection>
        </CardSection>
        </Card>
      
    )
  }
}

const styles = StyleSheet.create({
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})

export default Register;