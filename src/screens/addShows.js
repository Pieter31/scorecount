import React, { Component } from 'react';
import {Button,View,StyleSheet,TextInput,FlatList,TouchableOpacity,Text} from 'react-native';
import {Card,CardSection} from '../components';
import {firebase,database} from '@react-native-firebase/database';
// import auth from '@react-native-firebase/auth';

const DATA = [
    {
        showID: '001',
        showTitle: 'The Bachelor',
        user1: 'Admin',
        user2: 'Test',
        user1Score: '1',
        user2Score: '2'
    },
    {
        showID: '002',
        showTitle: 'Survivor COVID-19',
        user1: 'Test',
        user2: 'Admin',
        user1Score: '2',
        user2Score: '4' 
    },
    {
        showID: '003',
        showTitle: 'Shark Tank',
        user1: 'Admin',
        user2: 'Test',
        user1Score: '0',
        user2Score: '1'
    },
    // {
    //     showID: '004',
    //     showTitle: 'Fokken iets',
    //     user1: 'Admin',
    //     user2: 'Test',
    //     user1Score: '10',
    //     user2Score: '10'
    // },
    // {

    //     showID: '005',
    //     showTitle: 'Something',
    //     user1: 'Admin',
    //     user2: 'Ass',
    //     user1Score: 6969,
    //     user2Score: 420420
    // },
    {

        showID: '006',
        showTitle: 'test',
        user1: 'test',
        user2: 'Admin',
        user1Score:'10',
        user2Score:'11'
    }
]

class addShows extends Component {
    state ={showName:'',compEmail:'',currentUser:'',errorMessage:null,selectedShow:null}

    componentDidMount() {

        const{currentUser} = firebase.auth();
        this.setState({currentUser});
    }
    

    testCompName = () =>{

        console.log(this.state.compEmail);
        console.log(this.state.showName);
    }

    selectShow=(item)=>{
        // const {selectedShow} = item.showID;
        // console.log(selectedShow)
        // this.setState({selectedShow});
    }

    addShow = () => {
        // const{showName,currentUser,compEmail} = this.state;
        // const ref = database().ref(`/users/${compEmail}`);

        // console.log(ref)

        // if (currentUser == ''||showName=='') {

        //     this.setState({errorMessage:'Show name or Companion email has been left blank'})

        // } else {

           // ref.set({})

        // }
    }

    removeShow = () => {
        const {selectedShow} = this.state

        if (selectedShow == null) {
            this.setState({errorMessage: 'Please select a show to delete'})
            console.log(errorMessage)
        } else {
            console.log(selectedShow)
        }
    }

    render() {

        return(
            <View style={styles.container}>

                <View style={styles.viewContainer}>

                    <CardSection>

                        <CardSection>

                            <TextInput
                                style={styles.textInput}
                                placeholder="Show name"
                                onChangeText={showName => this.setState({ showName })}
                                value={this.state.showName}
                            />

                            <TextInput
                                style={styles.textInput}
                                placeholder= "Companion email"
                                onChangeText={compEmail => this.setState({ compEmail })}
                                value={this.state.compEmail}
                            />

                        </CardSection>

                        <CardSection>  

                            <Button
                                title='Add Show'
                                onPress={this.testCompName}
                            />

                        </CardSection>

                    </CardSection>

                </View>

                <View style={styles.viewContainer}>

                    {/* <CardSection> */}
                        <FlatList
                            data={DATA}
                            renderItem={({item}) =>
                                <View style={styles.container2ElectricBoogaloo}>
                                    <TouchableOpacity
                                        style={styles.listContainer} 
                                        onPress={this.selectShow(item)}
                                    >
                                        <Text>{item.showTitle}</Text>
                                    </TouchableOpacity>
                                </View>

                            }
                            keyExtractor={item => item.showID}
                        />
                    {/* </CardSection> */}

                    <CardSection>
                        <Button
                            title='Remove Show'
                            onPress={this.removeShow}
                        />
                    </CardSection>
                    
                </View>
            </View>
        )

    }

}

export default addShows;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "column"

    },
    textInput: {
        height: 40,
        width: '90%',
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 8
    },
    listContainer:{

        flexDirection: 'row',
        alignSelf: 'stretch',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,
        padding: 5,
        paddingTop: 10,
        paddingBottom: 10
    },
    container2ElectricBoogaloo:{
        padding: 5
    },
    viewContainer:{
        flex: 1,
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 5},

})